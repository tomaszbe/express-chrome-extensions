import { add4Hours, ajaxRequest, moveToWorkingDay, formatDateShort, branches } from './common.js'

function getProperty(propertyId) {
	var properties = masterViewModel.data.Wlasciwosci
	for(var i = 0; i < properties.length; ++i) {
		if(properties[i].Definicja.Id == propertyId) {
			return properties[i].Wartosc
		}
	}
	return null
}

function getDescription() {
	return getProperty(73)
}

function getWorkshop() {
	return getProperty(74)
}

function changeService(serviceOrderId, operatorId, serviceId, callback = null) {
	var url = 'http://rms.express/ZlecenieDTO/ChangeService/' + serviceOrderId
	var object = {
		errandId :serviceOrderId,
		operatorId: operatorId,
		serviceId: serviceId
	}

	ajaxRequest(url, object, callback)
}

function createServiceOrder(begin, notes, caseId, carId, description, personId, serwis) {
	var end = new Date(begin)
	var isOt = description.includes('OT')
	end.setHours(end.getHours() + 72)
	moveToWorkingDay(end)
	var object = {
	    DataRozpoczecia: begin,
	    PlanowanaDataZakonczenia: end,
	    Uwagi: isOt ? notes : '',
	    BlokujeSamochod: true,
	    SprawaId: caseId,
	    OpisZewnetrzny: description + '\n\n'
	     + (serwis == 'Altmot' ? 'Proszę odebrać auto z oddziału - Bokserska 64' : (serwis + ' - podstawi oddział')),
	    SamochodId: carId,
	    Kategoria: isOt ? 'Przegląd OT' : 'Mechaniczne',
	    Przebieg: masterViewModel.data.PrzebiegPoczatkowy,
	    Sprawa:{
	      Id: caseId
	    },
	    Samochod:{
	      Id: carId
	    },
	    SamochodId: carId,
	    Uzytkownik:{
	      Id: personId
	    },
	    UzytkownikId: personId
	  }

	ajaxRequest("/ZlecenieSerwisDTO/SaveItem", { object2Save: object, contextData: "" }, function (data) {
		var serviceOrderId = data.data.Id
		if (serwis != 'Altmot') {
			window.open('http://rms.express/ZlecenieSerwisDTO/Edit/' + serviceOrderId, '_blank')
		} else {
			changeService(serviceOrderId, 8, 252435, function() {
				createComment(':z ' + formatDateShort(begin))
				window.open('http://rms.express/ZlecenieSerwisDTO/Edit/' + serviceOrderId, '_blank')
			})
		}
		
	})
}

function createZt(begin, carId, caseFullId, fromId, toId, toAddress) {
	var end = add4Hours(begin)
	var object = {
		Z: { IdMiejscaWydania: fromId },
		Do: { IdMiejscaWydania: toId, Adres: { Adres1: toAddress } },
		DataRozpoczecia: begin,
		PlanowanaDataZakonczenia: end,
		Uwagi: 'serwisp ' + caseFullId,
		BlokujeSamochod: true,
		GodzinaAkcji: end,
		Kategoria: 'Transport logistyczny - transport między oddziałami',
		Samochod: {
			Id: carId
		},
		SamochodId: carId
	}

	ajaxRequest("/ZlecenieTransportoweDTO/SaveItem", { object2Save: object, contextData: "" }, function (data) {
		var ztUrl = 'http://rms.express/ZlecenieTransportoweDTO/Edit/' + data.data.Id
		createComment('<a href="' + ztUrl + '" target="_blank">' 
			+ 'serwisP '
			+ (end.getMonth() + 1) + '/' + end.getDate() + ' ' + end.getHours() + ':' + ( '0' + end.getMinutes()).slice(-2)
			+ '</a>')
		window.open(ztUrl, '_blank')
	})
}

function createComment(text) {
	var object = {
		CzyWymagaOdpowiedzi: '-1',
		IsZewnetrzny: false,
		OsobaKontaktowa: 'pitstop',
		Tresc: text,
		ReferencjaObiektu: {
			Id: masterViewModel.data.Id,
			Typ: 'Sprawa'
		}
	}

	ajaxRequest('/Komentarze/CreateItem', object, function() {
		$('[id^=komentarzeListView]')[0].kendoBindingTarget.target.dataSource.read()
	})
}

function sendZt() {
	var begin = new Date()
	var branch = masterViewModel.data.Oddzial.Id
	var carId = masterViewModel.data.SamochodId
	// var caseId = masterViewModel.data.Id
	var caseFullId = masterViewModel.data.NumerZewnetrzny
	var serwis = getWorkshop()
	var from = branches[branch].z
	var to = branches[branch].do

	createZt(begin, carId, caseFullId, from, to, serwis)
}

function sendServiceOrder() {
	var begin = new Date()
	var carId = masterViewModel.data.SamochodId
	var caseId = masterViewModel.data.Id
	var description = getDescription()
	var personId = masterViewModel.data.Zglaszajacy.Id
	var serwis = getWorkshop()

	createServiceOrder(begin, description.split('\n')[0], caseId, carId, description, personId, serwis)
}

(function() {
	document.addEventListener('sendZt', sendZt)
	document.addEventListener('sendServiceOrder', sendServiceOrder)
})()