import { add4Hours, formatDate } from './common.js'

function beginField() {
	return $('[data-property-name="DataRozpoczecia"]')[0]
}

function endField() {
	return $('[data-property-name="PlanowanaDataZakonczenia"]')[0]
}

function notesField() {
	return $('[data-property-name="Uwagi"]')[0]
}

function getCaseId() {
	var ref = masterViewModel.data.Sprawa
	return ref ? ref.DN : null
}

function fillBlanks() {
	var now = new Date()
	var later = add4Hours(now)
	beginField().value = formatDate(now)
	masterViewModel.data.DataRozpoczecia = now
	masterViewModel.data.PlanowanaDataZakonczenia = later
	endField().value = formatDate(later)
	var caseId = getCaseId()
	var typeLetter = /odbioru/.test(masterViewModel.data.Opis) ? 'o' : 'p'
	var racLetter = masterViewModel.data.LiniaProduktowa.Id === 1 /* RAC */
		? 'r' : ''
	var notes = 'serwis' + typeLetter + racLetter

	if (caseId) {
		notes += ' ' + caseId
	} 

	notesField().value = notes
	masterViewModel.data.Uwagi = notes
	masterViewModel.changeData = true
}

(function() {
	document.addEventListener('fillBlanks', fillBlanks)
})()