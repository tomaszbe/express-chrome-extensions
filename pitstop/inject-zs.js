function insertLink(id) {
  const url = '/ZlecenieTransportoweDTO/Edit/' + id
  const tbody = $('.itemSection:first-child() tbody')[0]
  const html = `
  <tr>
    <td colspan="4" align="center">
      <a href="${url}" style="font-size: 40px;" target="_blank">
        ZLECENIE PODSTAWIENIA
      </a>
    </td>
  </tr>`
  tbody.innerHTML = html + tbody.innerHTML
}


if (masterViewModel.data.ZlecenieTransportowePodstawienia.Id) {
  insertLink(masterViewModel.data.ZlecenieTransportowePodstawienia.Id)
}