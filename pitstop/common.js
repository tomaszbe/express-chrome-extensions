export function moveToWorkingDay(date) {
	while(date.getDay() == 0 || date.getDay() == 6) {
		date.setDate(date.getDate() + 1)
	}
}

export function add4Hours(date) {
	var later = new Date(date)

	if (date.getHours() >= 12) {
		later.setHours(date.getHours() + 20)
		if( later.getHours() >= 12) {
			later.setHours(12,0,0)
		}
	} else if (date.getHours() >= 8) {
		later.setHours(date.getHours() + 4)
	} else {
		later.setHours(12,0,0)
	}

	moveToWorkingDay(later)

	return later
}



export function ajaxRequest(url, object, callback = null) {
	$.ajax({
	    url: url,
	    type: "POST",
	    contentType: 'application/json',
	    data: JSON.stringify(object),
	    success: function (data) {
	    	if (callback) {
	    		callback(data)
	    	}
	    },
	    error: function(data = null) {
	    	// empty
	    }
	})

}

export function twoDigit(number) {
	return ('0' + number).slice(-2)
}

export function formatDate(date) {
	var string = ''
	string += twoDigit(date.getDate()) + '-'
	string += twoDigit(date.getMonth() + 1) + '-'
	string += date.getFullYear() + ' '
	string += twoDigit(date.getHours()) + ':'
	string += twoDigit(date.getMinutes())
	return string
}

export function formatDateShort(date) {
	var string = ''
	string += (date.getMonth() + 1) + '/'
	string += date.getDate() + ' '
	string += date.getHours() + ':'
	string += twoDigit(date.getMinutes())
	return string
}

export const branches = {
    1: {z: 1, do: 2},
    3: {z: 112, do: 18},
    7: {z: 111, do: 23},
    10: {z: 32, do: 28},
    12: {z: 73, do: 38},
    14: {z: 72, do: 47},
    16: {z: 156, do: 54},
    18: {z: 65, do: 61},
		22: {z: 155, do: 68},
		110: {z: 83, do: 43}, // Bydgoszcz
		111: {z: 70, do: 34} // Rzeszów
}