function createSectionElement(title) {
  let section = document.createElement('div')
  section.style.display = 'block'
  section.className = 'itemSection'
  let legend = document.createElement('legend')
  let titleElement = document.createElement('span')
  titleElement.innerText = title
  titleElement.style.marginLeft = '5px'
  legend.appendChild(titleElement)
  section.appendChild(legend)
  let content = document.createElement('div')
  content.className = 'preview'
  content.innerHTML = '<p>Analizuję styl jazdy...</p>'
  section.appendChild(content)
  return { section, content }
}

function addSection(title) {
  let panel = document.getElementById('sectionsPanel')
  let section = createSectionElement(title)
  panel.prepend(section.section)
  return section
}

function formatResponse(response) {
  if (response.text) {
    return `<p>${response.text}</p>`
  }
  if (response.length === 0) {
    return '<p>Nie znaleziono żadnych tras.</p>'
  }

  let out = ''

  for (let item of response.reverse()) {
    out += `<p>Z ${item.track.startAddr} do ${item.track.stopAddr}</p>`
    let status
    if (item.result.status === 'OK') {
      out += '<p>- Przejazd bez wykroczeń</p>'
    } else {
      const doubles = item.result.data.doubles
      out += `<p>- Podwójne przekroczenia prędkości: ${doubles.length}</p>`
      for (let double of doubles) {
        out += `<p>-- <b>${double.point.speed}</b> km/h na ograniczeniu do ${
          double.way.tags.maxspeed
        } km/h - <a target="_blank" href="https://www.google.pl/maps/search/${
          double.point.lat
        },${double.point.lon}">link</a></p>`
      }
      const over30s = item.result.data.over30s
      out += `<p>- Przekroczenia pow. 30 km/h: ${over30s.length}</p>`
      for (let over30 of over30s) {
        out += `<p>-- <b>${over30.point.speed}</b> km/h na ograniczeniu do ${
          over30.way.tags.maxspeed
        } km/h - <a target="_blank" href="https://www.google.pl/maps/search/${
          over30.point.lat
        },${over30.point.lon}">link</a></p>`
      }
      const aggresives = item.result.data.aggresive
      out += `<p>- Gwałtowne przeciążenia: ${aggresives.length}</p>`
    }
    out += '<br>'
  }

  return out
}

function getRequestData() {
 /// TODO: finish this. or not.
 // this should return {plate,start,end} object for $.get()
  const dataMap = {
    'RezerwacjaWydanieListDTO': {
      plate: 'NrRejestracyjny',
      start: 'DataWydania',
      end: 'DataZwrotu'
    },
    'ZlecenieTransportoweDTO': {
      plate: 'NumerRejestracyjny',
      start: 'FaktycznaDataRozpoczecia',
      end: 'FaktycznaDataZakonczenia'
    }

  }

  const viewType = location.pathname.split('/')[1]

}

async function analyse() {
  let start = masterViewModel.data.FaktycznaDataRozpoczecia
    ? masterViewModel.data.FaktycznaDataRozpoczecia
    : masterViewModel.data.DataWydania
  let end = masterViewModel.data.FaktycznaDataZakonczenia
    ? masterViewModel.data.FaktycznaDataZakonczenia
    : masterViewModel.data.DataZwrotu
  let plate = masterViewModel.data.NumerRejestracyjny
    ? masterViewModel.data.NumerRejestracyjny
    : masterViewModel.data.NrRejestracyjny
  console.log('Analysing...')
  let content = addSection('Analiza stylu jazdy').content
  try {
    let response = await $.get('http://localhost', { plate, start, end })
    content.innerHTML = formatResponse(response)
  } catch {
    content.innerHTML =
      '<p>Błąd analizy.</p>'
  }
}

document.addEventListener('speed', analyse)
