function injectScript (file_path, tag) {
    var node = document.getElementsByTagName(tag)[0]
    var script = document.createElement('script')
    // script.setAttribute('type', 'text/javascript')
    script.setAttribute('type', 'module')
    script.setAttribute('src', file_path)
    node.appendChild(script);
}

function getParent() {
	return document.querySelector('#contentRight div:nth-child(2)')
}

function createSection(titleText) {
	var parent = getParent()
	var section = document.createElement('div')
	section.classList.add('sectionUI')
	parent.appendChild(section)
	var title = document.createElement('span')
	title.innerText = titleText
	section.appendChild(title)
	var table = document.createElement('table')
	section.appendChild(table)
	var tbody = document.createElement('tbody')
	table.appendChild(tbody)
	return tbody
}

function createRow(tbody, button) {
	var tr = document.createElement('tr')
	var td = document.createElement('td')
	tbody.appendChild(tr)
	tr.appendChild(td)
	td.appendChild(button)
}

function createButton(tbody, text, callback) {
	var button = document.createElement('a')
	button.classList.value = 'sectionbutton k-button actionbutton Edit'
	button.innerText = text
	button.addEventListener('click', function () {
		if (!button.disabled) {
			callback()
			button.setAttribute('disabled', true)
		}
	})

	createRow(tbody, button)

	return button
}

function analyse() {
	console.log('Sending analyse request...')
	document.dispatchEvent(new Event('speed'))
}

(function() {
	var section = createSection('SPEED')
	createButton(section, 'Analizuj styl jazdy', analyse)
	injectScript(chrome.extension.getURL('inject.js'), 'body')
})()