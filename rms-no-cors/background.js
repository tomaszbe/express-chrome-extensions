function getTabOrigin(tab) {
  const urlParts = tab.url.split('/')
  return urlParts[0] + '//' + urlParts[2]
}

const tabOrigins = {}

chrome.tabs.query({}, function(results) {
  results.forEach(function(tab) {
    tabOrigins[tab.id] = getTabOrigin(tab)
  })
})

function onUpdatedListener(tabId, changeInfo, tab) {
  tabOrigins[tab.id] = getTabOrigin(tab)
}

function onRemovedListener(tabId) {
  delete tabOrigins[tabId]
}

chrome.tabs.onUpdated.addListener(onUpdatedListener)
chrome.tabs.onRemoved.addListener(onRemovedListener)

chrome.webRequest.onHeadersReceived.addListener(
  details => {
    if (tabOrigins[details.tabId] === undefined) {
      return {}
    }
    const corsHeaders = [
      {
        name: 'Access-Control-Allow-Headers',
        value: 'Content-type'
      },
      {
        name: 'Access-Control-Allow-Origin',
        value: tabOrigins[details.tabId]
      },
      {
        name: 'Access-Control-Allow-Credentials',
        value: 'true'
      }
    ]

    const corsHeaderNames = corsHeaders.map(header => header.name)

    let responseHeaders = details.responseHeaders.filter(
      header => corsHeaderNames.indexOf(header.name) === -1
    )
    
    responseHeaders = [...responseHeaders, ...corsHeaders]

    return { responseHeaders }
  },
  { urls: ['http://rms.express/*'] },
  ['blocking', 'responseHeaders']
)
